package main

/*
useful links
https://github.com/Shopify/sarama/blob/master/consumer.go#L12

mock data 
{ "userID" : 23, "message" : "hi welcome", "type" : "broadcast" }
{ "userID" : 24, "message" : "hi dcsd", "type" : "broadcast" }
{ "userID" : 25, "message" : "hi wcdsme", "type" : "verification" }
{ "userID" : 26, "message" : "hi wcsdcome", "type" : "broadcast" }
{ "userID" : 27, "message" : "hi welccscome", "type" : "broadcast" }
{ "userID" : 28, "message" : "hi come", "type" : "verification" }
{ "userID" : 29, "message" : "bye", "type" : "verification" }
{ "userID" : 29, "message" : "bye", "type" : "verifddd" }
{ "userID" : 30, "message" : "bye", "type" : "vfddd" }
INSERT INTO "user"(id, username, email, password)VALUES (23, 'skio', 'skio@dlo.com', 'xas');
*/

// email send : https://nathanleclaire.com/blog/2013/12/17/sending-email-from-gmail-using-golang/
// https://myaccount.google.com/lesssecureapps?pli=1

// Allow less secure apps: ON
import (
	"os"
	"os/signal"
	"github.com/Shopify/sarama"
	"encoding/json"
	"errors"
	"io/ioutil"
	_ "github.com/lib/pq"
	"database/sql"
	"log"
	"strconv"
	"net/smtp"
	"strings"
	"io"
	"fmt"
	"crypto/rand"
)

const (
	BROADCAST = "broadcast"
	VERIFICATION = "verification"
	//TOPICNAME = "test4"
	//BROKERADDRESS = "localhost:9092"
	//CONFIGFILEPATH = "C:/Users/gs-1421/go/src/github.com/assignment/conf/config.json"
	// shortcut way will work on it
	// for now please change the conf file path according to ur working directory
	CONFIGFILEPATH = "\\src\\assignment\\conf\\config.json"
	//CONFIGFILEPATH = "\\config.json"
)

type Message struct {
	UserID  int `json:"userID"`
	Message string `json:"message"`
	Type    string `json:"type"`
}
//	{"id" : 234,"username" : "rahul","password" : "3123dsaa","email" : "3e3@ccd.com"}

type User struct {
	ID       int `json:"id"`
	Username string `json:"username"`
	Password string `json:"password"`
	Email    string `json:"email"`
}

type Config struct {
	Topicname       string `json:"topicname"`
	Brokeraddress   string `json:"brokeraddress"`
	Dburl           string `json:"dburl"`
	Verificationurl string `json:"verificationurl"`
	Emailconfig     struct {
				Emailsender string `json:"emailsender"`
				Hosturl     string `json:"hosturl"`
				Hostport    string `json:"hostport"`
				Password    string `json:"password"`
			} `json:"emailconfig"`
}

var db *sql.DB // Note the sql package provides the namespace

var configuration Config

func dbInit() {

	var dberr error
	// Make sure not to shadow your global - just assign with = - don't initialise a new variable and assign with :=
	// creating db object
	db, dberr = sql.Open("postgres", configuration.Dburl + "?sslmode=disable")

	if dberr != nil {
		log.Fatal(dberr)
		panic(dberr)
	}
}

func configInit() {

	//file, err := ioutil.ReadFile("config.json")
	pwd, _ := os.Getwd()
	file, err := ioutil.ReadFile(pwd + CONFIGFILEPATH)
	if err != nil {
		//return err
		log.Fatal(err)
		panic(err)
	}

	//decoder := json.NewDecoder(file)
	//err = decoder.Decode(&configuration)
	err = json.Unmarshal(file, &configuration)

	if err != nil {
		//return err
		log.Fatal(err)
		panic(err)
	}

}

func main() {

	configInit();

	dbInit();

	config := sarama.NewConfig()
	// let conssumer return error	
	// by default it is false 
	config.Consumer.Return.Errors = true
	// Specify brokers address. This is default one
	brokers := []string{configuration.Brokeraddress}
	topic := configuration.Topicname
	// Create new consumer with above configurations
	master, err := sarama.NewConsumer(brokers, config)
	//  fail fast If Error occours
	if err != nil {
		panic(err)
	}
	// performed later in a program’s execution
	defer func() {
		if err := master.Close(); err != nil {
			panic(err)
		}
	}()
	consumer, err := master.ConsumePartition(topic, 0, sarama.OffsetOldest)
	//  fail fast If Error occours
	if err != nil {
		panic(err)
	}
	// We’ll create a channel to receive to recieve OS signals 
	signals := make(chan os.Signal, 1)
	// signal.Notify registers the given channel to receive notifications of the specified signals.
	signal.Notify(signals, os.Interrupt)
	// Get signnal for finish
	doneCh := make(chan int)

	go func() {
		for {
			//The select statement lets a goroutine wait on multiple communication operations.
			//A select blocks until one of its cases can run, then it executes that case. It chooses one at random if multiple are ready.
			select {
			case err := <-consumer.Errors():
				log.Print(err)
			case msg := <-consumer.Messages():

				log.Println("Received messages", string(msg.Key), string(msg.Value))
				processMessages(string(msg.Value));

			case <-signals:
				log.Println("Interrupt is detected")
			// push something in channel so that program execution finish
				doneCh <- 1
			}
		}
	}()
	// when doneCh get some value, println gets executed 
	<-doneCh
	log.Println("exit")
}

func processMessages(messages string) {

	go func() {
	if messageObj, e := validateMessage(messages); e != nil {
		log.Println("invalid Message :", e)
	} else {
		log.Println("Correct message format :", messageObj)

		if user, err := validateDBUser(messageObj.UserID); err != nil {
			log.Println("Cannot find user : ", err)
		} else {
			log.Println("user found : ", user)
			doFurtherProcess(messageObj, user);
		}
	}
	}()
}

func validateMessage(messages string) (Message, error) {

	messageObj := Message{}
	json.Unmarshal([]byte(messages), &messageObj)

	if (Message{}) == messageObj {
		return Message{}, errors.New("Please Check Message")
	} else if !(messageObj.Type == BROADCAST || messageObj.Type == VERIFICATION ) {
		return Message{}, errors.New("Please Check Message Type ")
	} else {
		return messageObj, nil
	}
}

func validateDBUser(userID int) (User, error) {

	selectQuery := "SELECT * FROM public.user WHERE id =" + strconv.Itoa(userID)
	rows, err := db.Query(selectQuery)

	if err != nil {
		log.Fatal(err)
		return User{}, errors.New("Cannot find user in database")
	}
	//user := new(User)

	user := User{}
	if rows.Next() {
		err := rows.Scan(&user.ID, &user.Username, &user.Email, &user.Password)
		if err != nil {
			log.Fatal(err)
			return User{}, errors.New("User found but Something went wrong while scanning user")
		}
	}
	//fmt.Printf("%d, %s, %s, %s", user.ID, user.Username, user.Password, user.Email)
	if user == (User{}) {
		return User{}, errors.New("Cannot find user in database")
	}
	return user, nil
}

func insertUUID(UUID string, userID int) (error) {


	insertQuery := "INSERT INTO verificationcode(uuid, userid)VALUES (" + "'" + UUID + "'" + "," + strconv.Itoa(userID) + ");"

	rows, err := db.Query(insertQuery)

	if err != nil {
		log.Fatal(err)
		return errors.New("Failed to put UUID in db")
	}
	log.Print(rows.Next());

	return nil
}

func doFurtherProcess(message Message, user User) {

	if message.Type == BROADCAST {
		sendMail(strings.TrimSpace(user.Email), strings.TrimSpace(message.Message))
		log.Print("email is sent")
	} else {
		uuid, err := newUUID()
		if err != nil {
			log.Fatal("error: %v\n", err)
		} else {
			verificationURL := configuration.Verificationurl;
			finalVerificationURL := verificationURL + "/" + uuid
			log.Print(finalVerificationURL)

			if err := insertUUID(uuid, user.ID); err != nil {
				log.Fatal(err)

			} else {
				//log.Print(strings.Trim("sadas", "dsa"))
				sendMail(strings.TrimSpace(user.Email), finalVerificationURL)
				log.Print("email is sent")
			}
		}
	}
}

func sendMail(receiver string, messageBody string) {
	// Set up authentication information.
	auth := smtp.PlainAuth(
		"",
		configuration.Emailconfig.Emailsender,
		configuration.Emailconfig.Password,
		configuration.Emailconfig.Hosturl,
	)
	// Connect to the server, authenticate, set the sender and recipient,
	// and send the email all in one step.

	msg := []byte("To: " + receiver + "\r\n" +
		"Subject: assignment !\r\n" +
		"\r\n" +
		messageBody)

	err := smtp.SendMail(
		configuration.Emailconfig.Hosturl + ":" + configuration.Emailconfig.Hostport,
		auth,
		configuration.Emailconfig.Emailsender,
		[]string{receiver},
		msg,
	)
	if err != nil {
		log.Fatal(err)
	}
}

func newUUID() (string, error) {
	uuid := make([]byte, 16)
	n, err := io.ReadFull(rand.Reader, uuid)
	if n != len(uuid) || err != nil {
		return "", err
	}
	// variant bits; see section 4.1.1
	uuid[8] = uuid[8] &^ 0xc0 | 0x80
	// version 4 (pseudo-random); see section 4.1.3
	uuid[6] = uuid[6] &^ 0xf0 | 0x40
	return fmt.Sprintf("%x-%x-%x-%x-%x", uuid[0:4], uuid[4:6], uuid[6:8], uuid[8:10], uuid[10:]), nil
}
